<?php
use App\Routes;

require __DIR__ . '/vendor/autoload.php';
$index = new Routes();
$index->route();
