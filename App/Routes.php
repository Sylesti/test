<?php

namespace App;

use App\TodoQuerty;

class Routes
{
    function route()
    {
        $url = explode('/', $_SERVER['REQUEST_URI']);
        $resultTodo = new TodoQuery();
        $nameTodo = $url[1];
        switch ($nameTodo) {
            case 'get_all':
                $allTodo = $resultTodo->getAll();
                foreach ($allTodo as $row) {
                    print $row . "<br/>";
                }
                break;
            case 'get_todo':
                $idTodo = $url[2];
                if (is_numeric($idTodo) == false) {
                    echo "404: Не корректный запрос!";
                    break;
                }
                $getTodo = $resultTodo->getTodo((int)$idTodo);
                print $getTodo[0];
                break;
            default:
                echo "404: Не корректный запрос!";
        }
    }
}
