CREATE TABLE users (
  userId             BIGINT NOT NULL,
  userName           TEXT   NOT NULL
);
ALTER TABLE users
  ADD CONSTRAINT pkUser PRIMARY KEY ("userId");
INSERT INTO users (userId, userName) VALUES
  (1, 'Джон'),
  (2, 'Сара');

CREATE TABLE posts (
  postId             BIGINT NOT NULL,
  userId             BIGINT NOT NULL,
  postName           TEXT   NOT NULL,
  postText           TEXT   NOT NULL
);
ALTER TABLE posts
  ADD CONSTRAINT pkPost PRIMARY KEY (postId);
ALTER TABLE posts
  ADD CONSTRAINT fkPostUser FOREIGN KEY (userId) REFERENCES users (userId);
INSERT INTO posts (postId, userId, postName, postText) VALUES
  (1, 1, 'Море', 'бла бла'),
  (2, 1, 'Горы', 'бла бла бла'),
  (3, 1, 'Дом', 'бла');

CREATE TABLE comments (
  commentId        BIGINT NOT NULL,
  postId           BIGINT NOT NULL,
  userId           BIGINT NOT NULL,
  commentText      TEXT   NOT NULL
);
ALTER TABLE comments
  ADD CONSTRAINT pkComment PRIMARY KEY (commentId);
ALTER TABLE comments
  ADD CONSTRAINT fkCommentPost FOREIGN KEY (postId) REFERENCES posts (postId);
ALTER TABLE comments
  ADD CONSTRAINT fkCommentUser FOREIGN KEY (userId) REFERENCES users (userId);
INSERT INTO comments (commentId, postId, userId, commentText)
VALUES
  (1, 2, 1, 'Норм'),
  (1, 2, 3, 'Норм Норм');







