<?php

namespace App;

use App\Connection;

class TodoQuery
{
    public function getAll(): array
    {
        $link = new Connection();
        $result = $link->connect()->query("SELECT name from todo");
        $getAll = array();
        while ($row = $result->fetch()) {
            $getAll[] = $row['name'];
        }
        return $getAll;
    }

    public function getTodo(int $idTodo): ?array
    {
        $result = null;
        $link = new Connection();
        $todoRow = $link->connect()->query("SELECT name from todo WHERE id=$idTodo")->fetch(\PDO::FETCH_NUM);
        if ($todoRow) {
            $result = $todoRow;
        }
        return $result;
    }
}