<?php

namespace App;

class Connection
{
    public function connect(): \PDO
    {
        $iniArray = parse_ini_file("config.ini");
        $dsn = $iniArray['dsn'];
        $user = $iniArray['user'];
        $pass = $iniArray['pass'];
        try {
            $pdo = new \PDO($dsn, $user, $pass);
        } catch (\PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        return $pdo;
    }
}
